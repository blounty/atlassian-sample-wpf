﻿using System.Windows;
using Atlassian.Sample.Core.ViewModels;
using TinyIoC;

namespace Atlassian.Sample.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            TinyIoCContainer.Current.Register<BaseViewModel>(new StageOneViewModel(), "StageOneView");
            TinyIoCContainer.Current.Register<BaseViewModel>(new StageTwoViewModel(), "StageTwoView");
            TinyIoCContainer.Current.Register<BaseViewModel>(new FileInfoViewModel(), "FileInfoView");
        }
    }
}