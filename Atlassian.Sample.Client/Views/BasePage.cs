﻿using System.Windows.Controls;
using Atlassian.Sample.Core.ViewModels;
using TinyIoC;

namespace Atlassian.Sample.Client.Views
{
    public abstract class BasePage
        : Page
    {
        public BasePage()
        {
            this.SetViewModel();
        }

        public BaseViewModel ViewModel { get; set; }

        protected virtual void SetViewModel()
        {
            var viewTypeName = this.GetType().Name;
            var viewModel = TinyIoCContainer.Current.Resolve<BaseViewModel>(viewTypeName);

            if (viewModel == null)
                return;

            this.ViewModel = viewModel;
            this.DataContext = this.ViewModel;
        }
    }
}