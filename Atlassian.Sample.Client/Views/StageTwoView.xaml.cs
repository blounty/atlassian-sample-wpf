﻿using System.IO;
using System.Windows;
using Atlassian.Sample.Core.ViewModels;

namespace Atlassian.Sample.Client.Views
{
    public partial class StageTwoView : BasePage
    {
        public StageTwoView()
        {
            InitializeComponent();
            this.Loaded += this.StageTwoView_Loaded;
            this.StageTwoViewModel.OpenFilesAction = this.OpenNativeFiles;
        }

        public StageTwoViewModel StageTwoViewModel
        {
            get { return this.ViewModel as StageTwoViewModel; }
        }

        private void StageTwoView_Loaded(object sender, RoutedEventArgs e)
        {
            this.StageTwoViewModel.ShowFileInfoAction = this.ShowFileInfo;
        }

        private void CloseClick(object sender, RoutedEventArgs e)
        {
            var navWindow = Window.GetWindow(this);
            if (navWindow != null)
            {
                navWindow.Close();
            }
        }

        private void ShowFileInfo(FileInfo selectedFileInfo)
        {
            var navWindow = Window.GetWindow(this);
            if (navWindow != null)
            {
                var fileInfoWindow = new FileInfoView();
                fileInfoWindow.Owner = navWindow;
                fileInfoWindow.Show();
            }
        }

        private void OpenNativeFiles()
        {
            foreach (var item in this.FileList.SelectedItems)
            {
                var fileInfo = item as FileInfo;
                if (fileInfo != null)
                {
                    var runExplorer = new System.Diagnostics.ProcessStartInfo();
                    runExplorer.FileName = "explorer.exe";
                    runExplorer.Arguments = fileInfo.FullName;
                    System.Diagnostics.Process.Start(runExplorer);
                }
            }
        }
    }
}