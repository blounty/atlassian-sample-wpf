﻿using System.Windows;
using System.Windows.Navigation;
using Atlassian.Sample.Core.ViewModels;

namespace Atlassian.Sample.Client.Views
{
    public partial class StageOneView : BasePage
    {
        public StageOneView()
        {
            InitializeComponent();
            this.Loaded += this.StageOneView_Loaded;
        }

        public StageOneViewModel StageOneViewModel
        {
            get { return this.ViewModel as StageOneViewModel; }
        }

        private void StageOneView_Loaded(object sender, RoutedEventArgs e)
        {
            this.StageOneViewModel.DirectorySelectedAction = this.NavigateToStageTwo;
            var navWindow = Window.GetWindow(this) as NavigationWindow;
            if (navWindow != null)
            {
                navWindow.ShowsNavigationUI = false;
                navWindow.SizeToContent = SizeToContent.WidthAndHeight;
            }
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            var navWindow = Window.GetWindow(this) as NavigationWindow;
            if (navWindow != null)
            {
                navWindow.Close();
            }
        }

        private void NavigateToStageTwo()
        {
            var stageTwoView = new StageTwoView();
            this.NavigationService.Navigate(stageTwoView);
        }
    }
}