﻿using System.Windows;
using Atlassian.Sample.Core.ViewModels;
using TinyIoC;

namespace Atlassian.Sample.Client.Views
{
    public abstract class BaseWindow
        : Window
    {
        public BaseWindow()
        {
            this.SetViewModel();
        }

        public BaseViewModel ViewModel { get; set; }

        protected virtual void SetViewModel()
        {
            var viewTypeName = this.GetType().Name;
            var viewModel = TinyIoCContainer.Current.Resolve<BaseViewModel>(viewTypeName);

            if (viewModel == null)
                return;

            this.ViewModel = viewModel;
            this.DataContext = this.ViewModel;
        }
    }
}