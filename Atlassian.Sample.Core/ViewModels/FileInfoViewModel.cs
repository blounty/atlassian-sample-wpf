﻿using System.IO;
using Atlassian.Sample.Core.Messages;
using TinyIoC;
using TinyMessenger;

namespace Atlassian.Sample.Core.ViewModels
{
    public class FileInfoViewModel
        : BaseViewModel
    {
        private FileInfo fileInfo;
        private ITinyMessengerHub messengerHub;
        private TinyMessageSubscriptionToken subscriptionToken;

        public FileInfoViewModel()
        {
            this.messengerHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            this.subscriptionToken = this.messengerHub.Subscribe<SelectedFileMessage>(this.SelectedFileReceived);
        }

        public FileInfo FileInfo
        {
            get { return fileInfo; }
            set
            {
                fileInfo = value;
                this.OnPropertyChanged();
            }
        }

        private void SelectedFileReceived(SelectedFileMessage message)
        {
            this.FileInfo = message.SelectedFileInfo;
        }
    }
}