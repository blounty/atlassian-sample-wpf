﻿using System;
using System.IO;
using System.Windows.Input;
using Atlassian.Sample.Core.Framework;
using Atlassian.Sample.Core.Messages;

namespace Atlassian.Sample.Core.ViewModels
{
    public class StageOneViewModel
        : BaseViewModel
    {
        private string directoryPath;

        public StageOneViewModel()
        {
            this.ChooseDirectoryCommand = new RelayCommand(this.ChooseDirectory);
            this.DirectorySelectedCommand = new RelayCommand(this.DirectorySelected,
                obj => Directory.Exists(this.DirectoryPath));
        }

        public Action DirectorySelectedAction { get; set; }

        public string DirectoryPath
        {
            get { return directoryPath; }
            set
            {
                directoryPath = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged("DirectoryIsValid");
            }
        }

        public ICommand ChooseDirectoryCommand { get; set; }

        public ICommand DirectorySelectedCommand { get; set; }

        private void ChooseDirectory(object parameter)
        {
            var directoryDialog = new System.Windows.Forms.FolderBrowserDialog();
            var result = directoryDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.DirectoryPath = directoryDialog.SelectedPath;
            }
        }

        private void DirectorySelected(object parameter)
        {
            if (this.DirectorySelectedAction != null)
                this.DirectorySelectedAction();

            var messenger = TinyIoC.TinyIoCContainer.Current.Resolve<TinyMessenger.ITinyMessengerHub>();
            messenger.Publish(new SelectedDirectoryMessage(this, this.DirectoryPath));
        }
    }
}