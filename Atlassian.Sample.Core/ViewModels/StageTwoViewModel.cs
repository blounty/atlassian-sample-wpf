﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Atlassian.Sample.Core.Framework;
using Atlassian.Sample.Core.Messages;
using TinyIoC;
using TinyMessenger;

namespace Atlassian.Sample.Core.ViewModels
{
    public class StageTwoViewModel
        : BaseViewModel
    {
        private ICommand copyItemToClipboardCommand;
        private string currentSearchingDirectory;
        private string directoryPath;
        private CancellationTokenSource directorySearchCancellationTokenSource;
        private ObservableCollection<FileInfo> files;
        private string filterString;
        private bool isSearching;
        private ITinyMessengerHub messengerHub;
        private ICommand openItemInNativeCommand;
        private bool searchSubDirectories;
        private ICommand searchSubDirectoriesCommand;
        private FileInfo selectedFileInfo;
        private ICommand showItemDetailCommand;
        private ICommand showItemInExplorerCommand;
        private TinyMessageSubscriptionToken subscriptionToken;

        public StageTwoViewModel()
        {
            this.Files = new ObservableCollection<FileInfo>();
            this.messengerHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            this.subscriptionToken =
                this.messengerHub.Subscribe<SelectedDirectoryMessage>(this.SelectedDirectoryReceived);
            this.SearchSubDirectoriesCommand = new RelayCommand(this.ToggleSubDirectoriesOfSelectedDirectory);
            this.ShowItemInExplorerCommand = new RelayCommand(this.ShowItemInExplorer,
                obj => this.SelectedFileInfo != null);
            this.OpenItemInNativeCommand = new RelayCommand(this.OpenItemInNative, obj => this.SelectedFileInfo != null);
            this.ShowItemDetailCommand = new RelayCommand(this.ShowItemDetail, obj => this.SelectedFileInfo != null);
            this.CopyItemToClipboardCommand = new RelayCommand(this.CopyItemToClipboard,
                obj => this.SelectedFileInfo != null);
        }


        public Action OpenFilesAction { get; set; }

        public string DirectoryPath
        {
            get { return directoryPath; }
            set
            {
                directoryPath = value;
                this.OnPropertyChanged();
            }
        }

        public string FilterString
        {
            get { return filterString; }
            set
            {
                filterString = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged("Files");
            }
        }

        public Action<FileInfo> ShowFileInfoAction { get; set; }

        public ICommand SearchSubDirectoriesCommand
        {
            get { return searchSubDirectoriesCommand; }
            set
            {
                searchSubDirectoriesCommand = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand ShowItemInExplorerCommand
        {
            get { return showItemInExplorerCommand; }
            set
            {
                showItemInExplorerCommand = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand OpenItemInNativeCommand
        {
            get { return openItemInNativeCommand; }
            set
            {
                openItemInNativeCommand = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand ShowItemDetailCommand
        {
            get { return showItemDetailCommand; }
            set
            {
                showItemDetailCommand = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand CopyItemToClipboardCommand
        {
            get { return copyItemToClipboardCommand; }
            set
            {
                copyItemToClipboardCommand = value;
                this.OnPropertyChanged();
            }
        }

        public bool SearchSubDirectories
        {
            get { return searchSubDirectories; }
            set
            {
                searchSubDirectories = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsSearching
        {
            get { return isSearching; }
            set
            {
                isSearching = value;
                this.OnPropertyChanged();
            }
        }

        public string CurrentSearchingDirectory
        {
            get { return currentSearchingDirectory; }
            set
            {
                currentSearchingDirectory = value;
                this.OnPropertyChanged();
            }
        }

        public ObservableCollection<FileInfo> Files
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FilterString))
                    return
                        new ObservableCollection<FileInfo>(
                            files.Where(fi => fi.Name.ToLower().Contains(this.FilterString.ToLower())));

                return files;
            }
            set { files = value; }
        }

        public FileInfo SelectedFileInfo
        {
            get { return selectedFileInfo; }
            set
            {
                selectedFileInfo = value;
                this.OnPropertyChanged();
            }
        }

        private void SelectedDirectoryReceived(SelectedDirectoryMessage message)
        {
            this.DirectoryPath = message.SelectedDirectory;
            this.messengerHub.Unsubscribe<SelectedDirectoryMessage>(this.subscriptionToken);
            this.ToggleSubDirectoriesOfSelectedDirectory(false);
        }

        private void ShowCurrentSelectedDirectory()
        {
            this.Files.Clear();
            foreach (var filePath in Directory.GetFiles(this.DirectoryPath))
            {
                this.Files.Add(new FileInfo(filePath));
            }
        }

        private async void ToggleSubDirectoriesOfSelectedDirectory(object parameter)
        {
            this.IsSearching = true;
            var shouldShow = (bool) parameter;
            if (shouldShow)
            {
                this.directorySearchCancellationTokenSource = new CancellationTokenSource();
                await
                    Task.Factory.StartNew(
                        () => this.DirectorySearch(this.DirectoryPath, directorySearchCancellationTokenSource.Token),
                        directorySearchCancellationTokenSource.Token);
            }
            else
            {
                if (this.directorySearchCancellationTokenSource != null)
                {
                    this.directorySearchCancellationTokenSource.Cancel();
                }
                this.ShowCurrentSelectedDirectory();
            }
            this.IsSearching = false;
        }

        private void DirectorySearch(string directoryPath, CancellationToken cancelationToken)
        {
            try
            {
                foreach (string directory in Directory.GetDirectories(directoryPath))
                {
                    if (cancelationToken.IsCancellationRequested)
                        break;
                    this.CurrentSearchingDirectory = directoryPath;
                    foreach (string file in Directory.GetFiles(directory))
                    {
                        if (cancelationToken.IsCancellationRequested)
                            break;
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (!this.Files.Any(fi => fi.FullName == file))
                                this.Files.Add(new FileInfo(file));
                        });
                    }
                    Thread.Sleep(2000);
                    this.DirectorySearch(directory, cancelationToken);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        private void ShowItemInExplorer(object parameter)
        {
            var runExplorer = new System.Diagnostics.ProcessStartInfo();
            runExplorer.FileName = "explorer.exe";
            runExplorer.Arguments = this.SelectedFileInfo.Directory.FullName;
            System.Diagnostics.Process.Start(runExplorer);
        }

        private void OpenItemInNative(object parameter)
        {
            if (this.OpenFilesAction != null)
            {
                this.OpenFilesAction();
            }
        }

        private void ShowItemDetail(object parameter)
        {
            if (this.ShowFileInfoAction != null)
            {
                this.ShowFileInfoAction(this.SelectedFileInfo);

                this.messengerHub.Publish(new SelectedFileMessage(this, this.SelectedFileInfo));
            }
        }

        private void CopyItemToClipboard(object parameter)
        {
            Clipboard.SetData(DataFormats.Text, this.SelectedFileInfo.FullName);
        }
    }
}