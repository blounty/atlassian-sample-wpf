﻿using System;
using System.Windows.Data;

namespace Atlassian.Sample.Core.Converters
{
    public class FileSizeConverter
        : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var length = (long) value;
            string sLen = length.ToString();

            if (length >= (1 << 30))
            {
                sLen = string.Format("{0} Gb", length >> 30);
            }
            else if (length >= (1 << 20))
            {
                sLen = string.Format("{0} Mb", length >> 20);
            }
            else if (length >= (1 << 10))
            {
                sLen = string.Format("{0} Kb", length >> 10);
            }
            else
            {
                sLen = string.Format("{0} bytes", length);
            }

            return sLen;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}