﻿using System.IO;
using TinyMessenger;

namespace Atlassian.Sample.Core.Messages
{
    public class SelectedFileMessage
        : TinyMessageBase
    {
        public SelectedFileMessage(object sender, FileInfo selectedFileInfo)
            : base(sender)
        {
            this.SelectedFileInfo = selectedFileInfo;
        }

        public FileInfo SelectedFileInfo { get; set; }
    }
}