﻿using TinyMessenger;

namespace Atlassian.Sample.Core.Messages
{
    public class SelectedDirectoryMessage
        : TinyMessageBase
    {
        public SelectedDirectoryMessage(object sender, string selectedDirectory)
            : base(sender)
        {
            this.SelectedDirectory = selectedDirectory;
        }

        public string SelectedDirectory { get; set; }
    }
}